public class Labo3 {

    public static void main(String[] args) {


//        zad 1
        {
            final float floatVar = 3.14F;
            final long longVar = 1000000000000000L;
        }
//        zad 2

        char charVar = 'a';
        int intVar = 10;

        charVar = (char) (charVar + intVar);  // char is promoted to int , casting needed to assign to char again
        intVar = charVar + intVar; // char is promoted to int, no need to cast

//          initializing float and double
        float floatVar = 3.14F;
        double doubleVar = 3.14;

        floatVar = (float) (floatVar + doubleVar); // float is promoted to double,  casting needed to assign to double var

//          initializing byte, int variable from scope is used
        byte byteVar = 8;

        byteVar = (byte) (intVar + byteVar); //// byte is promoted to int,  casting needed to assign to byte var


        //zad 3

        boolean isRaining = true;
        System.out.println(isRaining ? "Is raining" : "Is not raining");


//        zad 4


        int value = 10;
//            given:
//            A = [0, ∞)
//            B = (−∞, 1]
//            C = [0, 1]
        System.out.println(value < 0 ? "B" : value <= 1 ? "ABC" : "A");


//        zad 5

        byte x = 5;
        byte y = 10;

//        byte z = x + y;
//        short z = x + y;
        int z = x + y;
        long v = x + y;


    }


}
