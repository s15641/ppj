import java.util.Random;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class Labo5 {

    public static void main(String[] args) {

//        zad1
        boolean isRaining = false;
        boolean isSunny = true;

        if (isRaining && !isSunny) {
            System.out.println("slob");
        } else if (isRaining && isSunny) {
            System.out.println("rainbow");
        } else if (!isRaining && isSunny) {
            System.out.println("sunny");
        } else if (!isRaining && !isSunny) {
            System.out.println("overcast");
        }

//        zad2
        int value = isRaining ? 5 : 8;

//        zad3
        char hexInChar = 'A';
        System.out.println(Integer.parseInt(Character.toString(hexInChar), 16));

//        zad4
        for (int oddNumber = 0; oddNumber <= 100; oddNumber += 2) {
            System.out.print(oddNumber + " ");
        }
        System.out.println("/n");

//        zad5
        int s = 0;
        int counter = 1;
        while (counter <= 10) {
            s += counter;
            counter++;
        }

//        zad6

        for (int n = 1; n <= 10; n++) {
            System.out.print((1 / Math.pow(2, n)) + " ");
        }

        System.out.println();

//        zad7

//        version from exercise
        int count = 0;
        int num = 2;

        while (count != 10000) {
            int x = 2;
            while (num % x != 0) {
                x++;
            }
            if (num == x) {
                System.out.print(num + " ");
                count++;
                num++;
            } else {
                num++;
            }
        }

        System.out.println();


//       streams with optimization

        System.out.println(IntStream.iterate(2, n -> n +1)
                .filter(n -> n % 2 != 0 || n==2)
                .filter(n -> IntStream.range(2 , n)
                        .limit((int) Math.sqrt(n))
                        .noneMatch(divisor-> n % divisor ==0 ))
                .limit(10000)
                .boxed()
                .collect(toList()));

    }
}

